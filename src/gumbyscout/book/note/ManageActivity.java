package gumbyscout.book.note;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class ManageActivity extends Activity {
	private EditText editText_url;
	private EditText editText_title;
	private EditText editText_note;
	private Intent intent;
	private String keyUrl = "url";
	private String keyTitle = "title";
	private String keyNote = "note";
	private String keyIndex = "index";
	private String keyEdited = "edited";
	private boolean edited = false;
	private int editIndex;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        
        initializeWidgets();
        getIntentExtras();
        
    }
    
    public void initializeWidgets(){
    	editText_url = (EditText)findViewById(R.id.editText_url);
        editText_title = (EditText)findViewById(R.id.editText_title);
        editText_note = (EditText)findViewById(R.id.editText_note);
        intent = getIntent();

    }
    
    public void getIntentExtras(){
    	Bundle b = intent.getExtras();
    	if(b != null){
    		editText_title.setText(b.getString(keyTitle));
    		editText_note.setText(b.getString(keyNote));
    		editText_url.setText(b.getString(keyUrl));
    		editIndex = b.getInt(keyIndex);
            edited = true;
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_manage, menu);
        return true;
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
    	if(item.getItemId() == R.id.menu_openurl){
    		String url = editText_url.getText() + "";
    		Uri uri;
    		if(url.startsWith("http://")){
    			uri = Uri.parse(url);
    		}
    		else{
    			editText_url.setText("http://" + url);
    			uri = Uri.parse("http://" + url);
    		}
    		//open the intent to the URI, which happens to be a url
    		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    		startActivity(intent);
    		return true;
    	}
    	return super.onMenuItemSelected(featureId, item);
    }
    
    @Override
    public void onBackPressed() {
    	if(editText_note == null || editText_note.length() == 0){
    		intent.putExtra(keyNote, "EMPTY");
    	}
    	else{
    		intent.putExtra(keyNote, editText_note.getText() + "");
    	}
    	if(editText_title == null || editText_title.length() == 0){
    		intent.putExtra(keyTitle, "EMPTY");
    	}
    	else{
    		intent.putExtra(keyTitle, editText_title.getText() + "");
    	}
    	if(editText_url == null || editText_url.length() == 0){
    		intent.putExtra(keyUrl, "EMPTY");
    	}
    	else{
    		intent.putExtra(keyUrl, editText_url.getText() + "");
    	}
    	if(edited){
    		intent.putExtra(keyIndex, editIndex);	
    	}
    	intent.putExtra(keyEdited, edited);
    	
    	setResult(RESULT_OK, intent);
    	finish();
    	super.onBackPressed();
    }
}
