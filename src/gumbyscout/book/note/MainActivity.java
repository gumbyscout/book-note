package gumbyscout.book.note;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import android.net.Uri;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends ListActivity {
	
	private int INTENT_ADDBOOKMARK = 1;
	private int INTENT_EDITBOOKMARK = INTENT_ADDBOOKMARK + 1;
	private String keyUrl = "url";
	private String keyTitle = "title";
	private String keyNote = "note";
	private String keyIndex = "index";
	private String keyEdited = "edited";
	private String file = "data.dat";
	private ArrayList<Bookmark> bookmarks = new ArrayList<Bookmark>();;
	private ArrayAdapter<Bookmark> adapter;
	private FileInputStream in;
	private FileOutputStream out;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        readFile();
        
        adapter = new ArrayAdapter<Bookmark>(
        		this, android.R.layout.simple_list_item_1, bookmarks);
        
        setListAdapter(adapter);
        
        ListView view = getListView();
        registerForContextMenu(view);

    }
    
    public void readFile(){
    	try {
			in = openFileInput(file);
			Scanner scanner = new Scanner(in);
	        while(scanner.hasNextLine()){
	        	String url = scanner.nextLine();
	        	String title = scanner.nextLine();
	        	String note = scanner.nextLine();
	        	
	        	Bookmark bookmark = new Bookmark(url, title, note);
	        	bookmarks.add(bookmark);
	        }
	        scanner.close();
		} catch (FileNotFoundException e) {
		}
        
    }
    
    public void writeFile(){
    	try {
			out = openFileOutput(file, Context.MODE_PRIVATE);
			OutputStreamWriter writer = new OutputStreamWriter(out);
	    	BufferedWriter buf = new BufferedWriter(writer);
	    	PrintWriter printer = new PrintWriter(buf);
	    	
	    	for(int i = 0; i < bookmarks.size(); i ++){
	    		Bookmark item = bookmarks.get(i);
	    		printer.println(item.getUrl());
	    		printer.println(item.getTitle());
	    		printer.println(item.getNote());
	    	}
	    	printer.close();
	    	adapter.notifyDataSetChanged();
		} catch (FileNotFoundException e) {
		}
    	
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	Intent intent = new Intent(this, ManageActivity.class);
    	Bookmark bm = bookmarks.get(position);
    	intent.putExtra(keyTitle, bm.getTitle());
    	intent.putExtra(keyNote, bm.getNote());
    	intent.putExtra(keyUrl, bm.getUrl());
    	intent.putExtra(keyIndex, position);
    	startActivityForResult(intent, INTENT_EDITBOOKMARK);
    	super.onListItemClick(l, v, position, id);
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
    	if(item.getItemId() == R.id.menu_addbookmark){
    		Intent intent = new Intent(this, ManageActivity.class);
    		startActivityForResult(intent, INTENT_ADDBOOKMARK);
    		return true;
    	}
    	if(item.getItemId() == R.id.menu_clearbookmarks){
    		bookmarks.clear();
    		writeFile();
    		return true;
    	}
    	return super.onMenuItemSelected(featureId, item);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if(resultCode == RESULT_OK){
    		Bundle b = data.getExtras();
    		if(b != null){
    			String title = b.getString(keyTitle);
    			String url = b.getString(keyUrl);
    			String note = b.getString(keyNote);
    			boolean edited = b.getBoolean(keyEdited);
    			
    			Bookmark bookmark = new Bookmark(url, title, note);
    			if(edited){
    				int editIndex = b.getInt(keyIndex);
    				bookmarks.remove(editIndex);
    				bookmarks.add(editIndex, bookmark);
    			}
    			else{
    				bookmarks.add(bookmark);
    			}
    			writeFile();	
    		}
    	}
    	super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
    		ContextMenuInfo menuInfo) {
    	super.onCreateContextMenu(menu, v, menuInfo);
    	getMenuInflater().inflate(R.menu.context_main, menu);
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    	if(item.getItemId() == R.id.context_delete){
    		bookmarks.remove(info.position);
    		writeFile();
    	}
    	if(item.getItemId() == R.id.context_openUrl){
    		Bookmark thing = bookmarks.get(info.position);
    		
    		String url = thing.getUrl();
    		Uri uri;
    		if(url.startsWith("http://")){
    			uri = Uri.parse(url);
    		}
    		else{
    			thing.setUrl("http://" + url);
    			uri = Uri.parse("http://" + url);
    		}
    		//open the intent to the URI, which happens to be a url
    		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    		writeFile();
    		startActivity(intent);
    	}
    	return super.onContextItemSelected(item);
    }
}
