package gumbyscout.book.note;

public class Bookmark {
	private String url;
	private String title;
	private String note;

	public Bookmark(String url, String title, String note) {
		super();
		this.url = url;
		this.title = title;
		this.note = note;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String toString() {
		return this.title;
	}

}
